version:4.2.1
Millénaire: Normanska Invånare

By ledare

Chevalier:  Riddare. Bor i borgen, därifrån han går ut och patrullerar sitt land och besöker värdshuset. Se upp med att möta honom i strid! Leder soldaterna och borgens by

Abbé: Abbot. Bor i klostret och övervakar munkarna. Ledare över religösa byar och mindre byar.

Sénéchal :  Seneschal. Övervakar fälten i byn från sin herrgård. Head of agricultural hamlets and villages.

Maître de la Guilde : Gille mästare. Övervakar hatverkarnas tillverkning från gille huset. Ledare över små hantverkar byar och byar.
NEW_PAGE
Kvinnor

Villageoise:   Kvinnlig bybo. Nästan alla vuxna kvinnor är av denna sort. Deras roll innebär att hämta resurser insamlade från deras män och lämmna i stadshuset, bygga byggnader och att uppgradera äldre byggnader, samt att sälja och köpa varor från spelaren från olika platser.

Dame:  Dam. Bor i borgen med sin man, där hon skapar vävd gobeläng från ull till att dekorera borgen med samt att sälja till spelaren.
NEW_PAGE
Män

Fermier: Manlig bonde. Bor på gården, där han sår och skördar grödor.

Bûcheron: Timmerman bor i skogshuggarens stuga. Samlar trä och äpplen i skogsdungen planterar han träd och planterar träd plantor till att skapa mer träd.

Garde:  Vakt. Bor i vakthuset. Gör inget förrutom att dricka cider och be, såvida inte en bybo är attackerad. Inte en bra att vara fiende med, verkligen inte om han är beväpnad med skräckinjagande Normansk svärd.

Prêtre: Präst. Bor ensamm i prästgården. När han inte ber i kyrkan så sover han.

Forgeron: Smed. Bor i smedjan eller i vapenförrådet. Om järn är tillgängligt, så smider han verktyg om han är i smedjan och vapen om han är i vapenförrådet.

Eleveur : Djurbonde. Bor på boskapsgården, grisfarmen eller kyckling farmen. Samlar olika matrial från djur och gör steamy Norman tripes and tasty boudin noir.

Mineur :  Gruvarbetare. Bor och arbetar i gruvan, tröttsamt samlar in sten och sand. När hans gruva blir upgraderad med en ugn så smälter han dom till slät sten och glas.

Charpentier : Snickare. Bor och arbetar i snickarens hus, Där han gör träramar att användas till att bygga med.

Moine:  Munk. Bor i klostret där han mödosamt kopierar uråldriga böcker till pargament rullar. En konst enligt honom.

Marchand : Köpman. Går från värdshus till värdshus, byter varor från en by till en annan. Det är hans öde att resa omkring för alla bybors skull.

Marchand étranger : Utländsk köpman. Köpmän från fjärran ställen som besöker marknaden för att sälja sina exotiska varor, innan han lämmnar med dina hårt tjänade deniers.
NEW_PAGE
Barn

Garçon / fille : kille och tjej. Bor med sina föräldrar, växer på natten, tills de är gamla nog att flytta in i ett tomt hus och bli vuxena.