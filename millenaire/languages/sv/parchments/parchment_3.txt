version:1.1.4
Millénaire: Normanska föremål

Denier. Medeltids valuta som används av byborna. Finns i tre valörer: normala, argent (silver) värd 64 vanliga, och or (guld) som är värd 64 silver.

Cider äpple. Detta äpplet smlas in ac skogshuggaren och används till att göra cider och calva. Det kan köpas av spelaren som sedan bara kan sälja det till andra byar. 
NEW_PAGE
Mat föremål

Cider. Görs av byborna fråd cider äpplen denna uppfriskande dryck läker två hjärtan och kan användas tre gånger.

Calva. Mer känds som calvados. Denna äppel konjak från Normandie läker två hjärtan och kan användas tio gånger. Det görs från cider i tavernan.

Tripes. Denna berömda Normanska maträtt är bra när man har jobbat ute länge i kylan eller efter ett fall. Den läker fem hjärtan och kan användas tre gånger. Icke Normaner brukar gilla det mer innan de får veta vad det igentligen är. Du har blivit varnad.

Boudin noir. Nästan lika gott som tripes, boudin noir läker tre hjärtan och kan användas tre gånger. Precis som sin kusin Engelsk black pudding, så brukar det tyckas om mer innan folk får reda på hur det görs.
NEW_PAGE
Verktyg & vapen

Normansk spade. Denna högkvalitets spade är lika hållbar som en av järn men lika hållbar som en av guld. Det ända valet för en seriös grävare, Tillgänglig vid en smedja nära dig. 

Normansk hacka- Denna hacka hackar lika snabbt som en av guld och håller lika mycket som en av järn. Perfekt till dessa långa expeditioner! Var försiktig den kan inte hacka obsidian.

Normansk yxa. Lika snabb som den av guld lika hållbar som den av järn, Den har en seriös kraft speciellt i händerna på en arg skogshuggare.

Normansk flåhacka. Ungefär som en diamant flåhacka men ser mycket häftigare ut.

Normansk rustning. Den Normanska rustningen ger 50% mer skydd än diamant och får dig att se farligare ut istället för att se ut som man ska på en kostym fest.

Normansk svärd. Dyr väldigt dyr och värd varenda denier när du möter en tuff fiende.
NEW_PAGE
Construction & decoration

Timmer ram. Detta byggnads material består av ramar av mörkt trä som omger en kärna av jord eller okokt tegel. Gjord av snickaren för använding till byns byggnader eller att byta med spelaren.

Normansk gobeläng. Gjorda för hand av fortets dam. Dessa häftiga gobelänger kommer att dekorera de mest praktfulla bankett hallar. Designen är från  Bayeux gobelängerna, med den största som är 15*3 avbildning av William the Conqueror flotta som seglar mot England.
NEW_PAGE
Amuletter

Amulet av Yggrasil. Ingraverad med Yggrasil, världs trädet från Nordisk mytologi vars rötter når världens botten och vars grenar når toppen av världen. Denna amulet ser till att du alltid vet hur högt eler djupt du är.

Amulet av korpen. Normanska präster må vara lojala till kyrkan och påven i Rom men de har inte glömt de uråldriga vikinga traditionerna... Denna amulet dedikerad till Odin kommer att varna dig genom att bli röd som blod om ett onaturlig varelse är nära. Användbar till att undivka dom eller att hitta dom.

Amulet av dvärgarna. Vikingarna tog med sig denna till de Normanska stränderna, Den ser ut som Tors hammare och kommer att guida dig till jordens rikedommar. 

Amulet av SKöll och Rati. Får sin kraft från  Sköll and Rati. De stora vargarna som jagar solen och månen,Denna amulet tillåter bäraren att få dagen till natt och natten till dag. Men va varnad: dess kraft är inte oändlig.
NEW_PAGE
Pergament

Normanska bybor. Ett pergament som berättar om de olika byborna som kommer att döda dig om du slår en av dom.

Normanska byggnader. En beskrivning av de olika byggnaderna som du kan se byborna fastna vid när de bygger.

Normanska föremål. Du borde veta detta nu.

Hel Normansk rulle. Samma inehåll som de tidigare tre i en och samma rulle för att lättare ha med sig.